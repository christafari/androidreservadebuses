
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/04/2015 13:39:47
-- Generated from EDMX file: C:\Users\ing_christian\Desktop\Nueva carpeta (3)\WcfServiceBuses\WcfServiceBuses\Buses.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Buses];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Viaje_Agenda]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Viaje] DROP CONSTRAINT [FK_Viaje_Agenda];
GO
IF OBJECT_ID(N'[dbo].[FK_Viaje_Bus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Viaje] DROP CONSTRAINT [FK_Viaje_Bus];
GO
IF OBJECT_ID(N'[dbo].[FK_Reservas_Usuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservas] DROP CONSTRAINT [FK_Reservas_Usuario];
GO
IF OBJECT_ID(N'[dbo].[FK_Reservas_Viaje]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservas] DROP CONSTRAINT [FK_Reservas_Viaje];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Agenda]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Agenda];
GO
IF OBJECT_ID(N'[dbo].[Bus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bus];
GO
IF OBJECT_ID(N'[dbo].[Reservas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Reservas];
GO
IF OBJECT_ID(N'[dbo].[Usuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuario];
GO
IF OBJECT_ID(N'[dbo].[Viaje]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Viaje];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Agenda'
CREATE TABLE [dbo].[Agenda] (
    [id_agenda] int IDENTITY(1,1) NOT NULL,
    [Hora_partida] time  NOT NULL,
    [Fecha] datetime  NOT NULL,
    [duracion_viaje] varchar(50)  NOT NULL
);
GO

-- Creating table 'Bus'
CREATE TABLE [dbo].[Bus] (
    [Id_bus] int IDENTITY(1,1) NOT NULL,
    [capacidad] int  NOT NULL,
    [tipo] varchar(50)  NULL
);
GO

-- Creating table 'Reservas'
CREATE TABLE [dbo].[Reservas] (
    [id_reserva] int IDENTITY(1,1) NOT NULL,
    [cedula_usuario] int  NOT NULL,
    [id_viaje] int  NOT NULL,
    [puesto] int  NOT NULL,
    [fecha_reserva] datetime  NOT NULL
);
GO

-- Creating table 'Usuario'
CREATE TABLE [dbo].[Usuario] (
    [Cedula] int  NOT NULL,
    [Nombres] varchar(50)  NOT NULL,
    [Apellidos] varchar(50)  NOT NULL,
    [Telefono] int  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Viaje'
CREATE TABLE [dbo].[Viaje] (
    [id_viaje] int IDENTITY(1,1) NOT NULL,
    [Ciudad_origen] varchar(50)  NOT NULL,
    [Ciudad_destino] varchar(50)  NOT NULL,
    [id_bus] int  NOT NULL,
    [id_agenda] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id_agenda] in table 'Agenda'
ALTER TABLE [dbo].[Agenda]
ADD CONSTRAINT [PK_Agenda]
    PRIMARY KEY CLUSTERED ([id_agenda] ASC);
GO

-- Creating primary key on [Id_bus] in table 'Bus'
ALTER TABLE [dbo].[Bus]
ADD CONSTRAINT [PK_Bus]
    PRIMARY KEY CLUSTERED ([Id_bus] ASC);
GO

-- Creating primary key on [id_reserva] in table 'Reservas'
ALTER TABLE [dbo].[Reservas]
ADD CONSTRAINT [PK_Reservas]
    PRIMARY KEY CLUSTERED ([id_reserva] ASC);
GO

-- Creating primary key on [Cedula] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [PK_Usuario]
    PRIMARY KEY CLUSTERED ([Cedula] ASC);
GO

-- Creating primary key on [id_viaje] in table 'Viaje'
ALTER TABLE [dbo].[Viaje]
ADD CONSTRAINT [PK_Viaje]
    PRIMARY KEY CLUSTERED ([id_viaje] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [id_agenda] in table 'Viaje'
ALTER TABLE [dbo].[Viaje]
ADD CONSTRAINT [FK_Viaje_Agenda]
    FOREIGN KEY ([id_agenda])
    REFERENCES [dbo].[Agenda]
        ([id_agenda])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Viaje_Agenda'
CREATE INDEX [IX_FK_Viaje_Agenda]
ON [dbo].[Viaje]
    ([id_agenda]);
GO

-- Creating foreign key on [id_bus] in table 'Viaje'
ALTER TABLE [dbo].[Viaje]
ADD CONSTRAINT [FK_Viaje_Bus]
    FOREIGN KEY ([id_bus])
    REFERENCES [dbo].[Bus]
        ([Id_bus])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Viaje_Bus'
CREATE INDEX [IX_FK_Viaje_Bus]
ON [dbo].[Viaje]
    ([id_bus]);
GO

-- Creating foreign key on [cedula_usuario] in table 'Reservas'
ALTER TABLE [dbo].[Reservas]
ADD CONSTRAINT [FK_Reservas_Usuario]
    FOREIGN KEY ([cedula_usuario])
    REFERENCES [dbo].[Usuario]
        ([Cedula])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reservas_Usuario'
CREATE INDEX [IX_FK_Reservas_Usuario]
ON [dbo].[Reservas]
    ([cedula_usuario]);
GO

-- Creating foreign key on [id_viaje] in table 'Reservas'
ALTER TABLE [dbo].[Reservas]
ADD CONSTRAINT [FK_Reservas_Viaje]
    FOREIGN KEY ([id_viaje])
    REFERENCES [dbo].[Viaje]
        ([id_viaje])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reservas_Viaje'
CREATE INDEX [IX_FK_Reservas_Viaje]
ON [dbo].[Reservas]
    ([id_viaje]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------