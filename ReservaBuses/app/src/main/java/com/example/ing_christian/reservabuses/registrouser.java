package com.example.ing_christian.reservabuses;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.concurrent.ExecutionException;

public class registrouser extends Activity {
    TextView cedula;
    TextView nombres;
    TextView apellidos;
    TextView direccion;
    TextView telefono;
    TextView email;
    String Json;
    final static String dir = "com.example.ing_christian.reservabuses.Reserva";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrouser);

        cedula=(TextView) findViewById(R.id.txtcedula);
        nombres =(TextView) findViewById(R.id.txtnombre);
        apellidos= (TextView) findViewById(R.id.txtapellido);
        direccion =(TextView) findViewById(R.id.txtdireccion);
        telefono =(TextView) findViewById(R.id.txttelefo);
        email= (TextView) findViewById(R.id.txtemail);
        Intent in= getIntent();
        Json = in.getStringExtra("Json").toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registrouser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String  registrar_usuario(View v){

            String Namespace = "http://tempuri.org/";
            String metodName = "Agregar_usuario";
            SoapObject request = new SoapObject(Namespace, metodName);
            request.addProperty("ced", cedula.getText().toString());
            request.addProperty("no", nombres.getText().toString());
            request.addProperty("apell", apellidos.getText().toString());
            request.addProperty("dir", direccion.getText().toString());
            request.addProperty("tel", telefono.getText().toString());
            request.addProperty("em", email.getText().toString());

             WCFllamar a = new WCFllamar();
            a.recibir_request(request);
            a.execute();

            try {
                android.util.Log.d("json", a.get());
                Intent pasar = new Intent(this, Reservar.class);
                pasar.putExtra("Json", Json);
                startActivity(pasar);


            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();

            }


        return  null;
    }




}
