package com.example.ing_christian.reservabuses;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.concurrent.ExecutionException;

public class Consultar_reservas extends Activity {

    String[] J;

    ListView lista;
    int posicion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_reservas);
        lista =(ListView) findViewById(R.id.listView);
        lista.setAdapter(reser());
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long id) {
                Toast.makeText(getApplicationContext(), J[position], Toast.LENGTH_SHORT).show();
                posicion = position;
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Consultar_reservas.this);
                alertBuilder.setTitle("Desea eliminar la reserva");
                alertBuilder.setCancelable(true)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    JSONObject in=  new JSONObject (J[position]);
                                    eliminar(in.getString("Id"));
                                    Toast.makeText(getApplicationContext(),"Se ha eliminado su reserva",Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                alertBuilder.setCancelable(true)
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                Dialog dialog = alertBuilder.create();
                dialog.show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_consultar_reservas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ArrayAdapter<String> reser(){



        Intent rdatos= getIntent();
        JSONArray json;
        String[] datos =null;
        try {
            JSONObject js= new JSONObject(rdatos.getStringExtra(logg.dir1));
            json=js.getJSONArray("Lista");
            datos = new String[json.length()];
            J=new String[json.length()];

            for (int i = 0; i <json.length() ; i++) {
                JSONObject in= json.getJSONObject(i);
                J[i]=in.toString();
                String reserva= "Codigo reserva: "+  in.getString("Id")+ "\n"
                        + "Puesto: " + in.getString("Puesto") +"\n" +
                        "Ciudad_origen: " + in.getString("Ciudad_origen") +"\n" +
                        "Ciudad_destino: " + in.getString("Ciudad_destino") +"\n" +
                        "Fecha: " + in.getString("Fecha")

                        ;
                datos[i]=reserva;
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datos );
        return adaptador;

    }

   public void eliminar(String id_reserva){


       String Namespace = "http://tempuri.org/";
       String metodName = "eliminar_reserva";
       SoapObject request = new SoapObject(Namespace, metodName);
       request.addProperty("id_reserva", id_reserva);


       WCFllamar a = new WCFllamar();
       a.recibir_request(request);
       a.execute();

       try {
            a.get();
       } catch (InterruptedException e) {
           e.printStackTrace();
       } catch (ExecutionException e) {
           e.printStackTrace();

       }

   }

}
