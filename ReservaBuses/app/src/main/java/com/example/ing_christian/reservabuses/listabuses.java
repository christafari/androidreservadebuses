package com.example.ing_christian.reservabuses;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class listabuses extends Activity {
    ListView lista;
    final static String dir = "com.example.ing_christian.reservabuses.listabuses";
    final static String dir1 = "com.example.ing_christian.reservabuses.registrouser";

    String[] J;
    int posicion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listabuses);
        lista =(ListView) findViewById(R.id.list);
        lista.setAdapter(list_bus());

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                Toast.makeText(getApplicationContext(), J[position], Toast.LENGTH_SHORT).show();
                    posicion=position;


                    View view = (LayoutInflater.from(listabuses.this)).inflate(R.layout.user_input, null);

                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(listabuses.this);
                    alertBuilder.setView(view);
                alertBuilder.setTitle("Ingrese su numero de cedula");
                    final EditText userInput = (EditText) view.findViewById(R.id.userinput);

                    alertBuilder.setCancelable(true)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                   if(usuario(userInput.getText().toString()).equals("no existe el usuario")){
                                       Intent pasar = new Intent(getApplicationContext(), registrouser.class);
                                       pasar.putExtra("Json", J[posicion]);
                                       startActivity(pasar);
                                   }
                                    else{
                                       Intent pasar = new Intent(getApplicationContext(), Reservar.class);
                                       pasar.putExtra("Json", J[posicion]);
                                       pasar.putExtra("Cedula", userInput.getText());
                                       startActivity(pasar);
                                   }
                                }
                            });

                alertBuilder.setCancelable(true)
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                    Dialog dialog = alertBuilder.create();
                    dialog.show();
            }
        });
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_listabuses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ArrayAdapter<String> list_bus()  {
        Intent rdatos= getIntent();
        JSONArray json;
        String[] datos =null;
        try {
            JSONObject js= new JSONObject(rdatos.getStringExtra(logg.dir));
            json=js.getJSONArray("Lista");
            datos = new String[json.length()];
            J=new String[json.length()];

            for (int i = 0; i <json.length() ; i++) {
                JSONObject in= json.getJSONObject(i);
                J[i]=in.toString();
                String Bus= "Hora de Partida: "+  in.getString("Hora_partida")+ "\n"
                        + "Fecha: " + in.getString("Fecha")
                        ;
                datos[i]=Bus;
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datos );
        return adaptador;
    }

    public String usuario(String input){

        String resultado="";

        String Namespace = "http://tempuri.org/";
        String metodName = "Existe";
        SoapObject request = new SoapObject(Namespace, metodName);
        request.addProperty("cedula", input.toString());
        WCFllamar a = new WCFllamar();
        a.recibir_request(request);
        a.execute();

        try {
            Log.d("json", a.get());
            resultado=a.get().toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return  resultado;
    }
}

