package com.example.ing_christian.reservabuses;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.serialization.SoapObject;


import java.util.concurrent.ExecutionException;

public class logg extends Activity {

    TextView origen;
    TextView destino;
    final static String dir = "com.example.ing_christian.reservabuses.listabuses";
    final static String dir1 = "com.example.ing_christian.reservabuses.Consultar_reservas";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.principal);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logg, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void Log(View v) {
        origen = (TextView) findViewById(R.id.co);
        destino = (TextView) findViewById(R.id.cd);
        String Namespace = "http://tempuri.org/";
        String metodName = "retornarbuses";
        SoapObject request = new SoapObject(Namespace, metodName);
        request.addProperty("co", origen.getText().toString());
        request.addProperty("cd", destino.getText().toString());

        WCFllamar a = new WCFllamar();
        a.recibir_request(request);
        a.execute();
        try {
            Log.d("retorno", a.get());
            if(a.get().toString().equals("\""+"No hay disponibilidad"+"\"")){
                Toast.makeText(this,"No hay viajes disponibles",Toast.LENGTH_LONG).show();
            }
            else{
            Log.d("json", a.get());
            Intent pasar = new Intent(this, listabuses.class);
            pasar.putExtra(dir, a.get());
            startActivity(pasar);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();

        }

    }

    public void reservas(View v){
        View view = (LayoutInflater.from(logg.this)).inflate(R.layout.user_input, null);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(logg.this);
        alertBuilder.setView(view);
        alertBuilder.setTitle("Ingrese su numero de cedula");
        final EditText userInput = (EditText) view.findViewById(R.id.userinput);

        alertBuilder.setCancelable(true)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), (userInput.getText().toString()), Toast.LENGTH_LONG).show();
                        String resultado = cons(userInput.getText().toString());
                        Log.d("Reservas" ,resultado );
                        if (resultado.equals("\"" + "No hay reservas" + "\"")) {
                            Toast.makeText(getApplicationContext(), "No tiene reservas registradas", Toast.LENGTH_LONG).show();
                        } else {
                            Intent pasar = new Intent(getApplicationContext(), Consultar_reservas.class);
                            pasar.putExtra(dir1, cons(userInput.getText().toString()));
                            startActivity(pasar);
                        }

                    }
                });

        alertBuilder.setCancelable(true)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        Dialog dialog = alertBuilder.create();
        dialog.show();


    }

    public String cons(String id){

        String resultado="";
        String Namespace = "http://tempuri.org/";
        String metodName = "consultar_reservas";
        SoapObject request = new SoapObject(Namespace, metodName);
        request.addProperty("id", id);
        WCFllamar a = new WCFllamar();
        a.recibir_request(request);
        a.execute();

        try {
            resultado=a.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();

        }


        return  resultado;
    }


}






