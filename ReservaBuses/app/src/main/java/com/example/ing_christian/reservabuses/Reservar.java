package com.example.ing_christian.reservabuses;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.concurrent.ExecutionException;

public class Reservar extends Activity {

    ListView lista;
    String[] J;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservar);
        lista=(ListView) findViewById(R.id.listView2);
      //  lista.setAdapter(list_puestos());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reservar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public ArrayAdapter<String> list_puestos()  {
        Intent rdatos= getIntent();

        JSONArray json;
        String puestos="";
        String[] datos =null;
        try {
            JSONObject js= new JSONObject(rdatos.getStringExtra("Json"));
            json=js.getJSONArray("Lista");

            J=new String[json.length()];
            for (int i = 0; i <json.length() ; i++) {

                JSONObject in= json.getJSONObject(i);
                puestos= cargar_lugares((String)in.getString("id_viaje"),(String) in.getString("id_bus"));
                datos=new String[Integer.parseInt((String) in.getString("Capacidad"))];
                Toast.makeText(this,puestos,Toast.LENGTH_LONG).show();
            }

            JSONObject j =new JSONObject(puestos);
            JSONArray pue = j.getJSONArray("\"Estados\"");
            datos = new String[pue.length()];
            for (int i = 0; i < pue.length(); i++) {
                JSONObject in= json.getJSONObject(i);

                for (int k = 0; k <datos.length ; k++) {
                    datos[k]=in.getString("\"Estado\"");
                }


            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,datos );
        return adaptador;
    }



    public String cargar_lugares(String id_viaje,String id_bus){

        String resultado="";
        String Namespace = "http://tempuri.org/";
        String metodName = "disponibilidad_puestos";
        SoapObject request = new SoapObject(Namespace, metodName);
        request.addProperty("id_viaje", id_viaje);
        request.addProperty("id_bus", id_bus);
        WCFllamar a = new WCFllamar();
        a.recibir_request(request);
        a.execute();

        try {
            resultado=a.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();

        }


        return  resultado;
    }


}
